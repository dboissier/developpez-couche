var debug = false;

// contexte de la page
var ctx;

var idBoucleJeu;

// dimension du canvas
var screenWidth;
var screenHeight;

var jeuLance = false;

var NB_SAUCISSES = 4;
var saucisses = new Array(NB_SAUCISSES);

var boing = new Audio("./sounds/boing.wav");
var pouet = new Audio("./sounds/pouet.wav");
var panpan = new Audio("./sounds/panpan.wav");
//var musique = new Audio("./sounds/music.wav");
//musique.loop = 'loop';

var saucisseComestible = new Image();
saucisseComestible.src = "./images/saucisse.png";

var saucissePourrie = new Image();
saucissePourrie.src = "./images/saucisse_pourrie.png";

function BoiteCollision() {
    this.largeur = 0;
    this.hauteur = 0;
}

var soucoupe = {
    image:Image(),
    positionX:0,
    positionY:0,
    estTouchee:false,
    boiteCollision:{
        largeur:0,
        hauteur:0
    },
    collisionX:function () {
        return this.positionX - parseInt(this.image.width / 2) + parseInt((this.image.width - this.boiteCollision.largeur) / 2);
    },
    collisionY:function () {
        return this.positionY - parseInt(this.image.height / 2) + parseInt((this.image.height - this.boiteCollision.hauteur) / 2);
    }
};


function Saucisse() {
    this.positionX = 0;
    this.positionY = 0;
    this.pasDeDeplacement = 3;
    this.positionXParDefault = 600;
    this.estComestible = true;
    this.boiteCollision = new BoiteCollision();
}

Saucisse.prototype.collisionX = function () {
    return this.positionX + parseInt((saucisseComestible.width - this.boiteCollision.largeur) / 2);
};
Saucisse.prototype.collisionY = function () {
    return this.positionY + parseInt((saucisseComestible.height - this.boiteCollision.hauteur) / 2);
};

Saucisse.prototype.reinitialiser = function () {
    this.positionX = screenWidth + Math.floor(Math.random() * 201);
    this.estComestible = (Math.random() < 0.5);
};

function Tir() {
    this.image = new Image();
    this.positionX = 0;
    this.positionY = 0;
    this.pasDeDeplacement = 3;
    this.boiteCollision = new BoiteCollision();
}

Tir.prototype.collisionX = function () {
    return this.positionX + parseInt((this.image.width - this.boiteCollision.largeur) / 2);
};
Tir.prototype.collisionY = function () {
    return this.positionY + parseInt((this.image.height - this.boiteCollision.hauteur) / 2);
};

function Decor() {
    this.image = new Image();
    this.positionX = 0;
    this.positionY = 0;
    this.pasDeDeplacement = 0;
}
var ciel = new Decor();
var nuages = new Decor();
var plaine = new Decor();

var score = 0;

canardage = {};
canardage.Jeu = function () {

};


var tirs = [];

function initParams() {
    ctx = document.getElementById('canvas').getContext('2d');

    screenWidth = parseInt($("#canvas").attr("width"));
    screenHeight = parseInt($("#canvas").attr("height"));

    soucoupe.image.src = "./images/canardage_lapin.png";
    soucoupe.positionX = soucoupe.image.width;
    soucoupe.positionY = parseInt(screenHeight / 2);
    soucoupe.boiteCollision.largeur = soucoupe.image.width - 8;
    soucoupe.boiteCollision.hauteur = soucoupe.image.height - 40;

    ciel.image = new Image();
    ciel.image.src = "./images/ciel.jpg";

    nuages.image = new Image();
    nuages.image.src = "./images/ciel1.png";
    nuages.pasDeDeplacement = 1;

    plaine.image = new Image();
    plaine.image.src = "./images/ciel2.png";
    plaine.pasDeDeplacement = 2;

    $("#container").mousemove(function (e) {
        soucoupe.positionX = e.pageX;
        soucoupe.positionY = e.pageY;
    });

    $("#container").click(function () {
        tirer();
    });

    for (var i = 0; i < saucisses.length; i++) {
        var saucisse = new Saucisse();
        saucisse.positionX = saucisse.positionXParDefault + Math.floor(Math.random() * 201);
        saucisse.estComestible = (Math.random() < 0.5);
        saucisse.positionY = (i + 1) * 100;
        saucisse.boiteCollision.largeur = saucisseComestible.width - 8;
        saucisse.boiteCollision.hauteur = saucisseComestible.height - 4;

        saucisses[i] = saucisse;
    }
}


function lancerOuArreterJeu() {
    jeuLance = !jeuLance;

    if (jeuLance) {
        //arret de la fonction de dessin
        clearInterval(idBoucleJeu);
        //appel de la fonction dessiner toutes les 10 ms
//        musique.play();
        idBoucleJeu = setInterval(executer, 10);
    }
    else {
//        musique.pause();
        //arret de la fonction de dessin
        clearInterval(idBoucleJeu);
    }
}


function executer() {
    calculerDeplacementSaucisses();
    calculerDeplacementTirs();
    calculerDeplacementDecor();
    dessiner();
}


function tirer() {
    var tir = new Tir();
    tir.image.src = "./images/tir.png";
    tir.boiteCollision.largeur = 110;
    tir.boiteCollision.hauteur = 36;
    tir.positionX = soucoupe.positionX + parseInt(soucoupe.image.width / 2);
    tir.positionY = soucoupe.positionY;

    tirs.push(tir);

    panpan.play();
}

function calculerDeplacementSaucisses() {

    for (var i = 0; i < saucisses.length; i++) {
        var saucisse = saucisses[i];
        saucisse.positionX = saucisse.positionX - saucisse.pasDeDeplacement;
        if (saucisse.positionX < -saucisseComestible.width) {
            saucisse.positionX = screenWidth;
            //si le joueur oublie une saucisse alors il perd 2 points
            score -= 2;
            $("#score").text(score);
            pouet.play();
        }

        var collision = canardage.Jeu.prototype.collision(
            soucoupe.collisionX(), soucoupe.collisionY(), soucoupe.boiteCollision.largeur, soucoupe.boiteCollision.hauteur,
            saucisse.collisionX(), saucisse.collisionY(), saucisse.boiteCollision.largeur, saucisse.boiteCollision.hauteur);


        if (collision) {
            if (saucisse.estComestible) {
                //si le joueur touche une saucisse comestible alors il gagne 1 point
                score++;
                $("#score").text(score);
                boing.play();
                saucisse.reinitialiser();
            } else {
                soucoupe.estTouchee = true;
            }
        }
    }
}


function calculerDeplacementTirs() {
    for (var i = 0; i < tirs.length; i++) {
        var tirASupprimer = false;
        var tir = tirs[i];
        tir.positionX = tir.positionX + tir.pasDeDeplacement;

        if (tir.positionX > (screenWidth + tir.image.width)) {
            tirASupprimer = true;
        }


        for(var j= 0; j < saucisses.length; j++) {
            var saucisse = saucisses[j];
            if (!saucisse.estComestible) {
                var collision = canardage.Jeu.prototype.collision(
                    tir.collisionX(), tir.collisionY(), tir.boiteCollision.largeur, tir.boiteCollision.hauteur,
                    saucisse.collisionX(), saucisse.collisionY(), saucisse.boiteCollision.largeur, saucisse.boiteCollision.hauteur);
                if (collision) {
                    //si le joueur touche une saucisse non comestible alors il gagne 2 points
                    score+=2;
                    saucisse.reinitialiser();
                    tirASupprimer = true;
                    break;
                }
            }

        }
        if (tirASupprimer) {
            tirs.slice(i, 1);
        }
    }
}

canardage.Jeu.prototype.collision = function intersects(x1, y1, w1, h1, x2, y2, w2, h2) {
    if (x2 > (w1 + x1) || x1 > (w2 + x2))
        return false;
    if (y2 > (h1 + y1) || y1 > (h2 + y2))
        return false;
    return true;
};


function calculerDeplacementDecor() {
    nuages.positionX -= nuages.pasDeDeplacement;
    if (nuages.positionX == -nuages.image.width) {
        nuages.positionX = 0;
    }
    plaine.positionX -= plaine.pasDeDeplacement;
    if (plaine.positionX == -plaine.image.width) {
        plaine.positionX = 0;
    }
}

function dessiner() {
    ctx.clearRect(0, 0, screenWidth, screenHeight);
    ctx.save();

    //affichage du decor
    ctx.drawImage(ciel.image, 0, 0);

    dessinerDecorEnDeplacement(nuages);
    dessinerDecorEnDeplacement(plaine);

    // le lapin est centre sur les coordonnées du curseur
    ctx.drawImage(soucoupe.image, soucoupe.positionX - parseInt(soucoupe.image.width / 2), soucoupe.positionY - parseInt(soucoupe.image.height / 2));

    dessinerBoiteDeCollision(soucoupe.collisionX(), soucoupe.collisionY(), soucoupe.boiteCollision);

    //affichage des saucisses
    for (var i = 0; i < saucisses.length; i++) {
        ctx.drawImage(saucisses[i].estComestible ? saucisseComestible : saucissePourrie, saucisses[i].positionX, saucisses[i].positionY);

        dessinerBoiteDeCollision(saucisses[i].collisionX(), saucisses[i].collisionY(), saucisses[i].boiteCollision)
    }

    //affichage des tirs
    for (var j = 0; j < tirs.length; j++) {
        ctx.drawImage(tirs[j].image, tirs[j].positionX, tirs[j].positionY);

        dessinerBoiteDeCollision(tirs[j].collisionX(), tirs[j].collisionY(), tirs[j].boiteCollision)
    }

    ctx.restore();

    if (soucoupe.estTouchee) {
        jeuLance = !jeuLance;
        soucoupe.estTouchee = false;
        alert("Game Over");
        clearInterval(idBoucleJeu);
    }
}

function dessinerBoiteDeCollision(x, y, boiteCollision) {
    if (!debug) {
        return;
    }
    ctx.save();

    ctx.strokeStyle = '#f00';
    ctx.lineWidth = 2;
    ctx.strokeRect(x, y, boiteCollision.largeur, boiteCollision.hauteur);

    ctx.restore();
}

function dessinerDecorEnDeplacement(decor) {
    var decorImg = decor.image;
    var decorPositionX = decor.positionX;

    ctx.drawImage(decorImg, -decorPositionX, 0, decorImg.width + decorPositionX, decorImg.height, 0, 0, decorImg.width + decorPositionX, decorImg.height);
    ctx.drawImage(decorImg, 0, 0, -decorPositionX, decorImg.height, decorImg.width + decorPositionX, 0, -decorPositionX, decorImg.height);
}