
// Soucoupe 	108 	170 	256 	128
//Saucisse 1 	402 	100 	128 	41
//Saucisse 2 	425 	200 	128 	41
//Saucisse 3 	488 	300 	128 	41
//Saucisse 4 	363 	400 	128 	41



CanardageTest = TestCase("CanardageTest");
CanardageTest.prototype.testInsersect = function() {
    var jeu = new canardage.Jeu();
    var soucoupePositionX = 108;
    var soucoupePositionY = 170;

    var soucoupeLongueur = 256;
    var soucoupeHauteur = 128;

    var saucisseLongueur = 128;
    var saucisseHauteur = 41;
    assertFalse(jeu.collision(soucoupePositionX, soucoupePositionY, soucoupeLongueur, soucoupeHauteur, 402, 100, saucisseLongueur, saucisseHauteur));
    assertFalse(jeu.collision(soucoupePositionX, soucoupePositionY, soucoupeLongueur, soucoupeHauteur, 425, 200, saucisseLongueur, saucisseHauteur));
    assertFalse(jeu.collision(soucoupePositionX, soucoupePositionY, soucoupeLongueur, soucoupeHauteur, 488, 300, saucisseLongueur, saucisseHauteur));
    assertFalse(jeu.collision(soucoupePositionX, soucoupePositionY, soucoupeLongueur, soucoupeHauteur, 363, 400, saucisseLongueur, saucisseHauteur));
};

